package config

import (
	"github.com/BurntSushi/toml"
	"github.com/go-xweb/log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

type Config struct {
	BaseUrl   string
	Port      string
	Title     string
	Version   string
	Jwt       Jwt
	Databases map[string]Database
	Apps      map[string]App
	Test      int64
	Debug     bool
	Email     map[string]EmailProvider
	Modules   map[string]Module
	Raygun    Raygun
}
type Database struct {
	Type           string
	Server         string
	Port           string
	User           string
	Password       string
	Name           string
	Connection     string
	MaxConnections int
}
type EmailProvider struct {
	Smtp     string
	Port     int
	Username string
	Password string
	ApiKey   string
}

type Module struct {
	Name   string
	Active bool
}
type App struct {
	Name string
}
type Jwt struct {
	JwtKey        string
	JwtPayloadKey string
}

type Raygun struct {
	Key string
}

type Subdomains map[string]http.Handler

// Reads info from config file
func ReadConfig(configfile string) Config {
	absPath, _ := filepath.Abs(configfile)
	_, err := os.Stat(absPath)
	if err != nil {
		log.Fatal("Config file is missing: ", absPath)
	}
	var config Config
	if _, err := toml.DecodeFile(absPath, &config); err != nil {
		log.Fatal(err)
	}
	return config
}

func (subdomains Subdomains) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	domainParts := strings.Split(r.Host, ".")

	if mux := subdomains[domainParts[0]]; mux != nil {
		// Let the appropriate mux serve the request
		mux.ServeHTTP(w, r)
	} else {
		// Handle 404
		http.Error(w, "Not found", 404)
	}
}
