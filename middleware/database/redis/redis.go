package redis

import (
	"github.com/gorilla/context"
	"gopkg.in/redis.v5"
	"net/http"
)

func Middleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		client := redis.NewClient(&redis.Options{
			Addr:     "localhost:6379",
			Password: "", // no password set
			DB:       0,  // use default DB
		})
		context.Set(r, "mongo", client)
		h.ServeHTTP(w, r)
	})
}
