package orm

import (
	"bitbucket.org/paulocarvalhodesign/blazecore"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/context"
	"net/http"
)

func Middleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		Orm := blazecore.Orm
		context.Set(r, "orm", Orm)
		h.ServeHTTP(w, r)
	})
}
