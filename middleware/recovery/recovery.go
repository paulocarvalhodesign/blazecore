package recovery

import (
	"bitbucket.org/paulocarvalhodesign/blazecore"
	"bitbucket.org/paulocarvalhodesign/blazecore/utilities/panic"
	"net/http"
)

func Middleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		defer panicRecover.HandlePanic(blazecore.GetConfig())
		h.ServeHTTP(rw, req)
	})
}
