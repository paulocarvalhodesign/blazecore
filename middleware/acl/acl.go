package acl

import (
	"net/http"
)

type Permission struct {
	Id         int64
	RoleTypeId int64
	Name       string
	Read       int64
	Create     int64
	Delete     int64
	Active     int64
}

type Permissions []Permission

func Middleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		//user :=	context.Get(r, "authedUser").(auth.AuthedUserStruct)
		h.ServeHTTP(w, r)
	})
}
