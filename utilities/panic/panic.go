package panicRecover

import (
	"bitbucket.org/paulocarvalhodesign/blazecore/config"
	"log"
)

func HandlePanic(conf config.Config) {
	if err := recover(); err != nil {
	    log.Fatal(err)
	}
}

func ReportError(conf config.Config, message string) {
	log.Fatal(message)

}
