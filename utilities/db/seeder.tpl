package main

import (
	"fmt"
	"bitbucket.org/paulocarvalhodesign/blaze/utilities/db"
)

// Run the seeders
func main() {

	if len(seeder.Seeders) <= 0 {
		fmt.Println("No Seeders registered")
	}

	for _, s := range seeder.Seeders {
		err := s.Seed()
		if err != nil {
			fmt.Println(s.GetName(), " Failed - ", err.Error())
		} else {
			fmt.Println(s.GetName(), " - Done!")
		}
	}
}
