package email

import (
	"fmt"
	"gopkg.in/gomail.v2"
)

const HTML_TYPE = "text/html"
const PLAIN_TYPE = "text/plain"

type EmailPayload struct {
	From    string
	To      string
	Subject string
	Content string
	Type    string
}

type Config struct {
	Smtp     string
	Port     int
	Username string
	Password string
}

func DispatchEmail(payload EmailPayload, conf Config) {
	m := gomail.NewMessage()
	m.SetHeader("From", payload.From)
	m.SetHeader("To", payload.To)
	m.SetHeader("Subject", payload.Subject)
	if HTML_TYPE == payload.Type {
		m.SetBody(HTML_TYPE, payload.Content)
	} else {
		m.SetBody(PLAIN_TYPE, payload.Content)
	}

	// Send the email to Bob
	d := gomail.NewPlainDialer(conf.Smtp, conf.Port, conf.Username, conf.Password)
	if err := d.DialAndSend(m); err != nil {
		panic(err.Error())
	} else {
		fmt.Println("Email sent...")
	}
}
