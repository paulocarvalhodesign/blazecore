package blazecore

import (
"github.com/astaxie/beego/orm"
_ "github.com/go-sql-driver/mysql"
"github.com/go-xweb/log"
"net/http"
//"time"
"bitbucket.org/paulocarvalhodesign/blazecore/config"
)

var Conf config.Config
var Orm orm.Ormer

func SetConf(conf config.Config) {
	Conf = conf
}

func Run(srv *http.Server) {
	for _, db := range Conf.Databases {
		orm.RegisterDataBase(
			db.Connection,
			db.Type,
			db.User + ":" + db.Password + "@tcp(" + db.Server + ":" + db.Port + ")/" + db.Name + "?charset=utf8",
			db.MaxConnections)
	}
	Orm = orm.NewOrm()

	log.Fatal(srv.ListenAndServe())
}

func GetConfig() config.Config {
	return Conf
}
