package responses

type ApiResponse struct {
	Meta     interface{} `json:"meta"`
	Messages interface{} `json:"messages"`
	Data     interface{} `json:"data"`
}
